/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package datamanager;

/**
 *
 * @author akshay
 */
public class MaterialList {
    
    String materiallistid =null;
    String materialid =null;
    String typeid =null;
    String quantityid =null;

    public String getMateriallistid() {
        return materiallistid;
    }

    public void setMateriallistid(String materiallistid) {
        this.materiallistid = materiallistid;
    }

    public String getMaterialid() {
        return materialid;
    }

    public void setMaterialid(String materialid) {
        this.materialid = materialid;
    }

    public String getTypeid() {
        return typeid;
    }

    public void setTypeid(String typeid) {
        this.typeid = typeid;
    }

    public String getQuantityid() {
        return quantityid;
    }

    public void setQuantityid(String quantityid) {
        this.quantityid = quantityid;
    }

    
    
    
}
