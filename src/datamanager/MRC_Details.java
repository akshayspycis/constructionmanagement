/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package datamanager;

/**
 *
 * @author akshay
 */
public class MRC_Details {
    String mrc_id = null;
    String mrc_date = null;
    String mrc_no = null;
    String mrc_site = null;
    String supplierid = null;
    String matriallistid = null;
    String mrc_length = null;
    String mrc_width = null;
    String mrc_height = null;
    String mrc_measurement = null;
    String mrc_quantity = null;
    String mrc_time = null;
    String mrc_am_pm = null;
    String mrc_truckno = null;
    String mrc_royaltyno = null;

    public String getMrc_id() {
        return mrc_id;
    }

    public void setMrc_id(String mrc_id) {
        this.mrc_id = mrc_id;
    }

    public String getMrc_date() {
        return mrc_date;
    }

    public void setMrc_date(String mrc_date) {
        this.mrc_date = mrc_date;
    }

    public String getMrc_no() {
        return mrc_no;
    }

    public void setMrc_no(String mrc_no) {
        this.mrc_no = mrc_no;
    }

    public String getMrc_site() {
        return mrc_site;
    }

    public void setMrc_site(String mrc_site) {
        this.mrc_site = mrc_site;
    }

    public String getSupplierid() {
        return supplierid;
    }

    public void setSupplierid(String supplierid) {
        this.supplierid = supplierid;
    }

    public String getMatriallistid() {
        return matriallistid;
    }

    public void setMatriallistid(String matriallistid) {
        this.matriallistid = matriallistid;
    }

    public String getMrc_length() {
        return mrc_length;
    }

    public void setMrc_length(String mrc_length) {
        this.mrc_length = mrc_length;
    }

    public String getMrc_width() {
        return mrc_width;
    }

    public void setMrc_width(String mrc_width) {
        this.mrc_width = mrc_width;
    }

    public String getMrc_height() {
        return mrc_height;
    }

    public void setMrc_height(String mrc_height) {
        this.mrc_height = mrc_height;
    }

    public String getMrc_measurement() {
        return mrc_measurement;
    }

    public void setMrc_measurement(String mrc_measurement) {
        this.mrc_measurement = mrc_measurement;
    }

    public String getMrc_quantity() {
        return mrc_quantity;
    }

    public void setMrc_quantity(String mrc_quantity) {
        this.mrc_quantity = mrc_quantity;
    }

    public String getMrc_time() {
        return mrc_time;
    }

    public void setMrc_time(String mrc_time) {
        this.mrc_time = mrc_time;
    }

    public String getMrc_am_pm() {
        return mrc_am_pm;
    }

    public void setMrc_am_pm(String mrc_am_pm) {
        this.mrc_am_pm = mrc_am_pm;
    }

    public String getMrc_truckno() {
        return mrc_truckno;
    }

    public void setMrc_truckno(String mrc_truckno) {
        this.mrc_truckno = mrc_truckno;
    }

    public String getMrc_royaltyno() {
        return mrc_royaltyno;
    }

    public void setMrc_royaltyno(String mrc_royaltyno) {
        this.mrc_royaltyno = mrc_royaltyno;
    }

        
}
