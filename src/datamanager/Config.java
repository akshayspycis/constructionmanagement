package datamanager;

import admin.AdminHome;
import classmanager.ConfigMgr;
import classmanager.MRCDetailsMgr;
import classmanager.MaterialListMgr;
import classmanager.MaterialMgr;
import classmanager.MaterialTypeMgr;
import classmanager.QuantityMgr;
import classmanager.SupplierProfileMgr;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import modules.configuration.ProductConfiguration;
import modules.configuration.QuantityManagement;
import modules.configuration.ViewQuantity;
import modules.mrc_management.MRC_Details_Management;
import modules.mrc_management.NewMRC;
import modules.mrc_management.ViewMRC;
import modules.suppliermanagement.NewSupplier;
import modules.suppliermanagement.SupplierManagement;
import modules.suppliermanagement.ViewSupplier;

/**
 *
 * @author AMS
 */
public class Config {

    //database variables
    public static Connection conn = null;
    public static PreparedStatement pstmt = null;
    public static Statement stmt = null;
    public static ResultSet rs = null;
    public static String sql = null;
    
    //product variables
    //public static ArrayList<UserProfile> configuserprofile = null;
    public static ArrayList<SupplierProfile> configsupplierprofile = null;
    public static ArrayList<Quantity> configquantity = null;
    public static ArrayList<Material> configmaterial = null;
    public static ArrayList<MaterialType> configmaterialTypes = null;
    public static ArrayList<MaterialList> configmateriallist = null;
    
    //class managers
    public static ConfigMgr configmgr = null;
    public static SupplierProfileMgr supplierprofilemgr = null;
    public static QuantityMgr quantitymgr = null;
    public static MaterialMgr materialmgr = null;
    public static MaterialTypeMgr materialtypemgr = null;
    public static MaterialListMgr materiallistmgr = null;
    public static MRCDetailsMgr mrc_details_mgr = null;
    
    
        
    
    
    //forms
    public static ProductConfiguration productconfiguration = null;
    public static AdminHome adminhome = null;
    public static SupplierManagement suppliermanagement = null;
    public static NewSupplier newsupplier = null;
    public static ViewSupplier viewsupplier = null;
    public static QuantityManagement quantitymanagement = null;
    public static ViewQuantity viewquantity = null;
    public static MRC_Details_Management mrc_details_management = null;
    public static NewMRC new_mrc = null;
    public static ViewMRC view_mrc = null;
    
    
}
