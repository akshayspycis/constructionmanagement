/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package datamanager;

/**
 *
 * @author akshay
 */
public class MaterialType {
    String typeid =null;
    String materialid =null;
    String type =null;

    public String getTypeid() {
        return typeid;
    }

    public void setTypeid(String typeid) {
        this.typeid = typeid;
    }

    public String getMaterialid() {
        return materialid;
    }

    public void setMaterialid(String materialid) {
        this.materialid = materialid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    
    
}
