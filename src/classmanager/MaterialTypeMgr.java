/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package classmanager;

import datamanager.Config;
import datamanager.MaterialType;

/**
 *
 * @author akshay
 */
public class MaterialTypeMgr {
    
               //method to insert materialtypetype in database
    public boolean insMaterialType(MaterialType materialtype) {
        try {          
            Config.sql = "insert into materialtype ("
                    + "materialid,"
                    + "type)"
                    + "values (?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
           
            Config.pstmt.setString(1, materialtype.getMaterialid());
            Config.pstmt.setString(2, materialtype.getType());

            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.configmgr.loadMaterialType();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to materialtype in database 
   public boolean updMaterialType(MaterialType materialtype) {
        try {
            Config.sql = "update materialtype set"
                    + " materialid = ? ,"
                    + " type = ? "
                    + " where materialtypeid = '"+materialtype.getMaterialid()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
           Config.pstmt.setString(1, materialtype.getMaterialid());
            Config.pstmt.setString(2, materialtype.getType());          
                            
            int x = Config.pstmt.executeUpdate();
           
            if (x>0) {
                
                Config.configmgr.loadMaterialType();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete materialtype in database
   public boolean delMaterialType(String typeid)
   {
        try {          
            Config.sql = "delete from materialtype where typeid = '"+typeid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            if (Config.pstmt.executeUpdate()>0) {
                Config.configmgr.loadMaterialType();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
    
    
}
