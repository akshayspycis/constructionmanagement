/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package classmanager;

import datamanager.Config;
import datamanager.MaterialList;

/**
 *
 * @author akshay
 */
public class MaterialListMgr {
    
               //method to insert materiallist in database
    public boolean insMaterialList(MaterialList materiallist) {
        try {          
            Config.sql = "insert into materiallist ("
                    + "materialid ,"      
                    + "typeid ,"      
                    + "quantityid)"
                    + "values (?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, materiallist.getMaterialid());
            Config.pstmt.setString(2, materiallist.getTypeid());
            Config.pstmt.setString(3, materiallist.getQuantityid());

            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.configmgr.loadMaterialList();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to materiallist in database 
   public boolean updMaterialList(MaterialList materiallist) {
        try {
            Config.sql = "update materiallist set"
                    + " materialid = ? ,"
                    + " typeid = ? ,"
                    + " quantityid = ? "
                    + " where materiallistid = '"+materiallist.getMateriallistid()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, materiallist.getMaterialid());
            Config.pstmt.setString(2, materiallist.getTypeid());
            Config.pstmt.setString(3, materiallist.getQuantityid());         
                            
            int x = Config.pstmt.executeUpdate();
           
            if (x>0) {
                
                Config.configmgr.loadMaterialList();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete materiallist in database
   public boolean delMaterialList(String materiallistid)
   {
        try {          
            Config.sql = "delete from materiallist where materiallistid = '"+materiallistid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            if ( Config.pstmt.executeUpdate()>0) {
                Config.configmgr.loadMaterialList();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
    
    
}
