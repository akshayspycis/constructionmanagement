package classmanager;

import datamanager.Config;
import datamanager.SupplierProfile;

public class SupplierProfileMgr {
           //method to insert supplierprofile in database
    public boolean insSupplierProfile(SupplierProfile supplierprofile) {
        try {          
            Config.sql = "insert into supplierprofile ("
                   
                    + "suppliername ,"                    
                    + "gender ,"
                    + "contactno ,"
                    + "organization ,"
                    + "tinno ,"
                    + "address ,"
                    + "locality ,"                    
                    + "city ,"
                    + "state,"
                    + "pincode,"
                    + "email ,"
                    + "fax,"
                     + "website,"
                    + "other)"
                    
                    + "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, supplierprofile.getSuppliername());
            Config.pstmt.setString(2, supplierprofile.getGender());
            Config.pstmt.setString(3, supplierprofile.getContactno());
            Config.pstmt.setString(4, supplierprofile.getOrganization());
            Config.pstmt.setString(5, supplierprofile.getTinno());
            Config.pstmt.setString(6, supplierprofile.getAddress());
            Config.pstmt.setString(7, supplierprofile.getLocality());
            Config.pstmt.setString(8, supplierprofile.getCity());
            Config.pstmt.setString(9, supplierprofile.getState());
            Config.pstmt.setString(10, supplierprofile.getPincode());
            Config.pstmt.setString(11, supplierprofile.getEmail());
            Config.pstmt.setString(12, supplierprofile.getFax());
            Config.pstmt.setString(13, supplierprofile.getWebsite());            
            Config.pstmt.setString(14, supplierprofile.getOther());
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.configmgr.loadSupplierProfile();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to supplierprofile in database 
   public boolean updSupplierProfile(SupplierProfile supplierprofile) {
        try {
            Config.sql = "update supplierprofile set"
                    + " suppliername = ?, "
                    + " gender = ?, "
                    + " contactno = ?,"
                    + " organization = ?, "
                    + " tinno = ?, "                    
                    + " address = ?, "
                    + " locality = ?, "
                    + " city = ?, "
                    + " state = ?, "
                    + " pincode = ?, "
                    + " email = ?, "
                    + " fax = ?, "
                    + " website = ?, "
                    + " other = ? "
                    + " where supplierid = '"+supplierprofile.getSupplierid()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, supplierprofile.getSuppliername());
            Config.pstmt.setString(2, supplierprofile.getGender());
            Config.pstmt.setString(3, supplierprofile.getContactno());
            Config.pstmt.setString(4, supplierprofile.getOrganization());
            Config.pstmt.setString(5, supplierprofile.getTinno());
            Config.pstmt.setString(6, supplierprofile.getAddress());
            Config.pstmt.setString(7, supplierprofile.getLocality());
            Config.pstmt.setString(8, supplierprofile.getCity());
            Config.pstmt.setString(9, supplierprofile.getState());
            Config.pstmt.setString(10,supplierprofile.getPincode());
            Config.pstmt.setString(11,supplierprofile.getEmail());
            Config.pstmt.setString(12,supplierprofile.getFax());
            Config.pstmt.setString(13,supplierprofile.getWebsite());            
            Config.pstmt.setString(14,supplierprofile.getOther());                
                            
            int x = Config.pstmt.executeUpdate();
           
            if (x>0) {
                
                Config.configmgr.loadSupplierProfile();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete supplierprofile in database
   public boolean delSupplierProfile(String supplierid)
   {
        try {          
            Config.sql = "delete from supplierprofile where supplierid = '"+supplierid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.configmgr.loadSupplierProfile();
                  
                
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
}
