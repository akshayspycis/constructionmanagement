/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package classmanager;

import datamanager.MRC_Details;
import datamanager.Config;

/**
 *
 * @author akshay
 */
public class MRCDetailsMgr {

    //method to insert mrcdetails in database
    
    public boolean insMRC_Details(MRC_Details mrcdetails) {
        try {          
            Config.sql = "insert into mrcdetails ("
                    + "mrc_date ,"      
                    + "mrc_no ,"      
                    + "mrc_site ,"      
                    + "supplierid ,"  
                    + "matriallistid ,"  
                    + "mrc_length ,"      
                    + "mrc_width ,"  
                    + "mrc_height ,"      
                    + "mrc_measurement ,"  
                    + "mrc_quantity ,"      
                    + "mrc_time ,"  
                    + "mrc_am_pm ,"  
                    + "mrc_truckno ,"      
                    + "mrc_royaltyno)"
                    + "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, mrcdetails.getMrc_date());
            Config.pstmt.setString(2, mrcdetails.getMrc_no());
            Config.pstmt.setString(3, mrcdetails.getMrc_site());
            Config.pstmt.setString(4, mrcdetails.getSupplierid());
            Config.pstmt.setString(5, mrcdetails.getMatriallistid());
            Config.pstmt.setString(6, mrcdetails.getMrc_length());
            Config.pstmt.setString(7, mrcdetails.getMrc_width());
            Config.pstmt.setString(8, mrcdetails.getMrc_height());
            Config.pstmt.setString(9, mrcdetails.getMrc_measurement());
            Config.pstmt.setString(10, mrcdetails.getMrc_quantity());
            Config.pstmt.setString(11, mrcdetails.getMrc_time());
            Config.pstmt.setString(12, mrcdetails.getMrc_am_pm());
            Config.pstmt.setString(13, mrcdetails.getMrc_truckno());
            Config.pstmt.setString(14, mrcdetails.getMrc_royaltyno());

            if (Config.pstmt.executeUpdate()>0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to mrcdetails in database 
   public boolean updMRC_Details(MRC_Details mrcdetails) {
        try {
            Config.sql = "update mrcdetails set"
                    + " mrc_date = ? ,"
                    + " mrc_no = ? ,"
                    + " mrc_site = ? ,"
                    + " supplierid = ? ,"
                    + " matriallistid = ? ,"
                    + " mrc_length = ? ,"
                    + " mrc_width = ? ,"
                    + " mrc_height = ? ,"
                    + " mrc_measurement = ? ,"
                    + " mrc_quantity = ? ,"
                    + " mrc_time = ? ,"
                    + " mrc_am_pm = ? ,"
                    + " mrc_truckno = ? ,"
                    + " mrc_royaltyno = ? "
                    + " where mrc_id = '"+mrcdetails.getMrc_id()+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, mrcdetails.getMrc_date());
            Config.pstmt.setString(2, mrcdetails.getMrc_no());
            Config.pstmt.setString(3, mrcdetails.getMrc_site());
            Config.pstmt.setString(4, mrcdetails.getSupplierid());
            Config.pstmt.setString(5, mrcdetails.getMatriallistid());
            Config.pstmt.setString(6, mrcdetails.getMrc_length());
            Config.pstmt.setString(7, mrcdetails.getMrc_width());
            Config.pstmt.setString(8, mrcdetails.getMrc_height());
            Config.pstmt.setString(9, mrcdetails.getMrc_measurement());
            Config.pstmt.setString(10, mrcdetails.getMrc_quantity());
            Config.pstmt.setString(11, mrcdetails.getMrc_time());
            Config.pstmt.setString(12, mrcdetails.getMrc_am_pm());
            Config.pstmt.setString(13, mrcdetails.getMrc_truckno());
            Config.pstmt.setString(14, mrcdetails.getMrc_royaltyno());
            
            if ( Config.pstmt.executeUpdate()>0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete mrcdetails in database
   public boolean delMRC_Details(String mrc_id)
   {
        try {          
            Config.sql = "delete from mrcdetails where mrc_id = '"+mrc_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            if ( Config.pstmt.executeUpdate()>0) {
                Config.configmgr.loadMaterialList();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
}
