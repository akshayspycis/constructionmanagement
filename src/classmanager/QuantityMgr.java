/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package classmanager;

import datamanager.Config;
import datamanager.Quantity;

/**
 *
 * @author Home
 */
public class QuantityMgr {
            //method to insert quantity in database
    public boolean insQuantity(Quantity quantity) {
        try {          
            Config.sql = "insert into quantity ("
                    + "quantitytype)"
                    + "values (?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, quantity.getQuantitytype());

            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.configmgr.loadQuantity();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to quantity in database 
   public boolean updQuantity(Quantity quantity) {
        try {
            Config.sql = "update quantity set"
                    + " quantitytype = ? "
                    + " where quantityid = '"+quantity.getQuantityid()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, quantity.getQuantitytype());                
                            
            int x = Config.pstmt.executeUpdate();
           
            if (x>0) {
                
                Config.configmgr.loadQuantity();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete quantity in database
   public boolean delQuantity(String quantityid)
   {
        try {          
            Config.sql = "delete from quantity where quantityid = '"+quantityid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.configmgr.loadQuantity();
                  
                
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
}
