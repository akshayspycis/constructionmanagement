package classmanager;

import admin.AdminHome;
import datamanager.Config;
import datamanager.Material;
import datamanager.MaterialList;
import datamanager.MaterialType;
import datamanager.Quantity;
import datamanager.SupplierProfile;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import modules.configuration.ProductConfiguration;
import modules.configuration.QuantityManagement;
import modules.configuration.ViewQuantity;
import modules.mrc_management.MRC_Details_Management;
import modules.mrc_management.NewMRC;
import modules.mrc_management.ViewMRC;
import modules.suppliermanagement.NewSupplier;
import modules.suppliermanagement.SupplierManagement;
import modules.suppliermanagement.ViewSupplier;

/**
 *
 * @author AMS
 */
public class ConfigMgr {    

    public boolean loadDatabase() {
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            String userName = "root";
            String password = "root";
            String url = "jdbc:mysql://localhost:3307/shapersconstruction2";
            Config.conn = DriverManager.getConnection(url, userName, password);
            Config.stmt = Config.conn.createStatement();            
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
//    public boolean loadUserProfile() {
//        Config.configuserprofile = new ArrayList<UserProfile>();
//        try {
//            Config.sql = "select * from userprofile";
//            Config.rs = Config.stmt.executeQuery(Config.sql);
//            while (Config.rs.next()) {
//                UserProfile up = new UserProfile();
//                up.setUserid(Config.rs.getString("userid"));
//                up.setUsername(Config.rs.getString("username"));
//                up.setPassword(Config.rs.getString("password"));
//                up.setOwnername(Config.rs.getString("ownername"));
//                up.setGender(Config.rs.getString("gender"));
//                up.setContactno(Config.rs.getString("contactno"));
//                up.setOrganization(Config.rs.getString("organization"));
//                up.setTinno(Config.rs.getString("tinno"));
//                up.setAddress(Config.rs.getString("address"));
//                up.setLocality(Config.rs.getString("locality"));
//                up.setCity(Config.rs.getString("city"));
//                up.setPincode(Config.rs.getString("pincode"));
//                up.setState(Config.rs.getString("state"));
//                up.setEmail(Config.rs.getString("email"));
//                up.setFax(Config.rs.getString("fax"));
//                up.setWebsite(Config.rs.getString("website"));
//                up.setSequrityquestion(Config.rs.getString("sequrityquestion"));
//                up.setAnswer(Config.rs.getString("answer"));
//                up.setOther(Config.rs.getString("other"));
//                
//                Config.configuserprofile.add(up);
//            }                
//        } catch (SQLException ex) {
//            ex.printStackTrace();
//            return false;
//        }
//        return true;
//    }
//    
//    public boolean loadProduct() {
//        Config.configproduct = new ArrayList<Product>();
//        try {
//            Config.sql = "select * from product";
//            Config.rs = Config.stmt.executeQuery(Config.sql);
//            while (Config.rs.next()) {
//                Product p = new Product();                
//                p.setProductname(Config.rs.getString("productname"));     
//                
//                Config.configproduct.add(p);
//            }                
//        } catch (SQLException ex) {
//            ex.printStackTrace();
//            return false;
//        }
//        return true;
//    }
//    
//    public boolean loadFlavor() {
//        Config.configflavor = new ArrayList<Flavor>();
//        try {
//            Config.sql = "select * from flavor";
//            Config.rs = Config.stmt.executeQuery(Config.sql);
//            while (Config.rs.next()) {
//                
//                Flavor f = new Flavor();
//                f.setFlavorname(Config.rs.getString("flavorname"));
//                f.setProductname(Config.rs.getString("productname"));
//                
//                Config.configflavor.add(f);
//            }
//        } catch (SQLException ex) {
//            ex.printStackTrace();
//            return false;
//        }
//        return true;
//    }
//    
//    public boolean loadScheme() {
//        Config.configscheme  = new ArrayList<Scheme>();
//        try {
//            Config.sql = "select * from scheme ";
//            Config.rs = Config.stmt.executeQuery(Config.sql);
//            while (Config.rs.next()) {
//                Scheme s = new Scheme();                
//                s.setSchemeid(Config.rs.getString("schemeid"));     
//                s.setProduct(Config.rs.getString("product"));     
//                s.setFlavor(Config.rs.getString("flavor"));     
//                s.setMrp(Config.rs.getString("mrp"));     
//                s.setAmount(Config.rs.getString("amount"));     
//                s.setPercentage(Config.rs.getString("percentage"));     
//
//                Config.configscheme.add(s);
//            }                
//        } catch (SQLException ex) {
//            ex.printStackTrace();
//            return false;
//        }
//        return true;
//    }
//    
//    public boolean loadCustomerProfile() {
//        Config.configcustomerprofile = new ArrayList<CustomerProfile>();
//        try {            
//            Config.sql = "select * from customerprofile";
//            Config.rs = Config.stmt.executeQuery(Config.sql);
//            while (Config.rs.next()) {                    
//                
//                CustomerProfile cp = new CustomerProfile();
//                cp.setCustomerid(Config.rs.getString("customerid"));                
//                cp.setCustomername(Config.rs.getString("customername"));
//                cp.setGender(Config.rs.getString("gender"));
//                cp.setContactno(Config.rs.getString("contactno"));
//                cp.setOrganization(Config.rs.getString("organization"));
//                cp.setTinno(Config.rs.getString("tinno"));
//                cp.setAddress(Config.rs.getString("address"));
//                cp.setLocality(Config.rs.getString("locality"));
//                cp.setCity(Config.rs.getString("city"));
//                cp.setPincode(Config.rs.getString("pincode"));
//                cp.setState(Config.rs.getString("state"));
//                cp.setEmail(Config.rs.getString("email"));
//                cp.setFax(Config.rs.getString("fax"));
//                cp.setWebsite(Config.rs.getString("website"));
//                cp.setOther(Config.rs.getString("other"));                
//                
//                Config.configcustomerprofile.add(cp);                
//            }                
//        } catch (SQLException ex) {
//          ex.printStackTrace();
//            return false;
//        }
//        return true;
//    }
//    
    public boolean loadSupplierProfile() {
        Config.configsupplierprofile = new ArrayList<SupplierProfile>();
        try {            
            Config.sql = "select * from supplierprofile";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {                    
                
                SupplierProfile cp = new SupplierProfile();
                cp.setSupplierid(Config.rs.getString("supplierid"));                
                cp.setSuppliername(Config.rs.getString("suppliername"));
                cp.setGender(Config.rs.getString("gender"));
                cp.setContactno(Config.rs.getString("contactno"));
                cp.setOrganization(Config.rs.getString("organization"));
                cp.setTinno(Config.rs.getString("tinno"));
                cp.setAddress(Config.rs.getString("address"));
                cp.setLocality(Config.rs.getString("locality"));
                cp.setCity(Config.rs.getString("city"));
                cp.setState(Config.rs.getString("state"));
                cp.setPincode(Config.rs.getString("pincode"));
                cp.setEmail(Config.rs.getString("email"));
                cp.setFax(Config.rs.getString("fax"));
                cp.setWebsite(Config.rs.getString("website"));
                cp.setOther(Config.rs.getString("other"));
                
                Config.configsupplierprofile.add(cp);                
            }                
        } catch (SQLException ex) {
          ex.printStackTrace();
            return false;
        }
        return true;
    }
//    
//    public boolean loadEmployeeProfile() {
//        Config.configemployeeprofile = new ArrayList<EmployeeProfile>();
//        try {            
//            Config.sql = "select * from employeeprofile";
//            Config.rs = Config.stmt.executeQuery(Config.sql);
//            while (Config.rs.next()) {
//                
//                EmployeeProfile ep = new EmployeeProfile();
//                ep.setEmployeeid(Config.rs.getString("employeeid"));
//                ep.setEmployeename(Config.rs.getString("employeename"));
//                ep.setGender(Config.rs.getString("gender"));
//                ep.setDob(Config.rs.getString("dob"));
//                ep.setAge(Config.rs.getString("age"));
//                ep.setFathername(Config.rs.getString("fathername"));
//                ep.setMaritalstatus(Config.rs.getString("maritalstatus"));
//                ep.setIdentitytype(Config.rs.getString("identitytype"));
//                ep.setIdentityno(Config.rs.getString("identityno"));
//                ep.setContactno(Config.rs.getString("contactno"));
//                ep.setAltcontactno(Config.rs.getString("altcontactno"));
//                ep.setAddress(Config.rs.getString("address"));
//                ep.setLocality(Config.rs.getString("locality"));
//                ep.setCity(Config.rs.getString("city"));
//                ep.setPincode(Config.rs.getString("pincode"));
//                ep.setState(Config.rs.getString("state"));
//                ep.setEmail(Config.rs.getString("email"));
//                ep.setSalary(Config.rs.getString("salary"));
//                ep.setJoiningdate(Config.rs.getString("joiningdate"));
//                ep.setLeavingdate(Config.rs.getString("leavingdate"));
//                ep.setOther(Config.rs.getString("other"));
//                
//                Config.configemployeeprofile.add(ep);                
//            }                
//        } catch (SQLException ex) {
//            ex.printStackTrace();
//            return false;
//        }
//        return true;
//    } 
//    
//     public boolean loadPayroll() {
//        Config.configpayroll = new ArrayList<PayRoll>();
//        try {            
//            Config.sql = "select * from payroll";
//            Config.rs = Config.stmt.executeQuery(Config.sql);
//            while (Config.rs.next()) {
//                
//                PayRoll ep = new PayRoll();
//                ep.setPayrollid(Config.rs.getString("payrollid"));
//                ep.setEmployeeid(Config.rs.getString("employeeid"));
//                ep.setMonth(Config.rs.getString("month"));
//                ep.setYear(Config.rs.getString("year"));
//                ep.setPayment(Config.rs.getString("payment"));
//                ep.setDate(Config.rs.getString("date"));
//                ep.setTime(Config.rs.getString("time"));
//                
//                Config.configpayroll.add(ep);                
//            }                
//        } catch (SQLException ex) {
//            ex.printStackTrace();
//            return false;
//        }
//        return true;
//    } 
//  
    public boolean loadClassManager() {
        try {
            Config.supplierprofilemgr = new SupplierProfileMgr();
            Config.materialmgr = new MaterialMgr();
            Config.materialtypemgr = new MaterialTypeMgr();
           // Config.customerprofilemgr = new CustomerProfileMgr();
           // Config.employeeprofilemgr = new EmployeeProfileMgr();

            Config.materiallistmgr = new MaterialListMgr();
            Config.quantitymgr = new QuantityMgr();
            Config.mrc_details_mgr = new MRCDetailsMgr();
           // Config.flavormgr = new FlavorMgr();
            //Config.schememgr = new SchemeMgr();

            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
//    
    public boolean loadForms() {
        try {
            Config.adminhome = new AdminHome();
            Config.suppliermanagement = new SupplierManagement(null, true);
            Config.newsupplier = new NewSupplier(null, true);
            Config.viewsupplier = new ViewSupplier(null, true);
            Config.productconfiguration = new ProductConfiguration(null, true);
            Config.quantitymanagement = new QuantityManagement(null, true);
            Config.viewquantity = new ViewQuantity(null, true);
            Config.mrc_details_management = new MRC_Details_Management(null, true);
            Config.new_mrc = new NewMRC(null, true);
            Config.view_mrc = new ViewMRC(null, true);
            
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }        
    }

    public boolean loadQuantity() {
        Config.configquantity = new ArrayList<Quantity>();
        try {            
            Config.sql = "select * from quantity";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {                    
                
                Quantity cp = new Quantity();
                cp.setQuantityid(Config.rs.getString("quantityid"));                
                cp.setQuantitytype(Config.rs.getString("quantitytype"));
                Config.configquantity.add(cp);                
            }                
        } catch (SQLException ex) {
          ex.printStackTrace();
            return false;
        }
        return true;
    }
        
       public boolean loadMaterial() {
        Config.configmaterial = new ArrayList<Material>();
        try {            
            Config.sql = "select * from material";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {                    
                
                Material m = new Material();
                m.setMaterialid(Config.rs.getString("materialid"));                
                m.setMaterialname(Config.rs.getString("materialname"));
                Config.configmaterial.add(m);                
            }                
        } catch (SQLException ex) {
          ex.printStackTrace();
            return false;
        }
        return true;
    }
     
       public boolean loadMaterialType() {
        Config.configmaterialTypes = new ArrayList<MaterialType>();
        try {            
            Config.sql = "select * from materialtype";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {                    
                
                MaterialType cp = new MaterialType();
                cp.setTypeid(Config.rs.getString("typeid"));                    
                cp.setMaterialid(Config.rs.getString("materialid"));                
                cp.setType(Config.rs.getString("type"));
                Config.configmaterialTypes.add(cp);                
            }                
        } catch (SQLException ex) {
          ex.printStackTrace();
            return false;
        }
        return true;
    } 
      
       public boolean loadMaterialList() {
        Config.configmateriallist = new ArrayList<MaterialList>();
        try {            
            Config.sql = "select * from materiallist";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {                    
                
                MaterialList cp = new MaterialList();
                cp.setMateriallistid(Config.rs.getString("materiallistid"));                    
                cp.setMaterialid(Config.rs.getString("materialid"));                
                cp.setTypeid(Config.rs.getString("typeid"));
                cp.setQuantityid(Config.rs.getString("quantityid"));
                Config.configmateriallist.add(cp);                
            }                
        } catch (SQLException ex) {
          ex.printStackTrace();
            return false;
        }
        return true;
    } 
}
