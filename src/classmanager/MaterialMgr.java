/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package classmanager;

import datamanager.Config;
import datamanager.Material;

/**
 *
 * @author akshay
 */
public class MaterialMgr {
              //method to insert material in database
    public boolean insMaterial(Material material) {
        try {          
            Config.sql = "insert into material ("
                    + "materialname)"
                    + "values (?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, material.getMaterialname());

            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.configmgr.loadMaterial();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to material in database 
   public boolean updMaterial(Material material) {
        try {
            Config.sql = "update material set"
                    + " materialname = ? "
                    + " where materialid = '"+material.getMaterialid()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, material.getMaterialname());                
                            
            int x = Config.pstmt.executeUpdate();
           
            if (x>0) {
                
                Config.configmgr.loadMaterial();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete material in database
   public boolean delMaterial(String materialid)
   {
        try {          
            Config.sql = "delete from material where materialid = '"+materialid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.configmgr.loadMaterial();
                  
                
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
}
