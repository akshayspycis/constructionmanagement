package shapersconstruction2;
/**
 *
 * @author AMS
 */
public class Banner extends javax.swing.JFrame {
    /**
     * Creates new form Banner
     */
    public Banner() {
        initComponents();        
        this.setLocationRelativeTo(null);
//        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/neuronimgs/nicon.png")));
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblShow = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(null);

        lblShow.setForeground(new java.awt.Color(51, 51, 51));
        lblShow.setText("...");
        getContentPane().add(lblShow);
        lblShow.setBounds(10, 210, 430, 20);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/banner.jpg"))); // NOI18N
        getContentPane().add(jLabel1);
        jLabel1.setBounds(0, 0, 601, 237);

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-601)/2, (screenSize.height-238)/2, 601, 238);
    }// </editor-fold>//GEN-END:initComponents
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel lblShow;
    // End of variables declaration//GEN-END:variables
    
    void setBannerLabel(String lable) {
        lblShow.setText(lable);
    }
}
