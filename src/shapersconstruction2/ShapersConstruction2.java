package shapersconstruction2;

import classmanager.ConfigMgr;
import datamanager.Config;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author AMS
 */
public class ShapersConstruction2 {

    Banner banner;

    public ShapersConstruction2() {
        banner = new Banner();
        Config.configmgr = new ConfigMgr();
    }    
    
    private void showbanner() {        
        banner.setVisible(true);
        banner.setBannerLabel("Checking System Configuration...");
    }
    
    private void connserver() {
        banner.setBannerLabel("Connecting To Server...");        
    }
    
    private void conndatabase() {
        banner.setBannerLabel("Checking Databse Connection...");
        if (!Config.configmgr.loadDatabase()) {
            JOptionPane.showMessageDialog(banner,"Database connectivity problem, product can not be launch.","Error : 10068.",JOptionPane.ERROR_MESSAGE);            
            System.exit(0);
        }
    }
    
    private void initcomponent() {
        banner.setBannerLabel("Initialising Components...");        
        int x=0;
//        if (Config.configmgr.loadUserProfile()){x++;} else {System.out.println("Error : 1, UserProfile loading problem.");}
          if (Config.configmgr.loadMaterialList()){x++;} else {System.out.println("Error : 2, Material loading problem.");}
//        if (Config.configmgr.loadFlavor()){x++;} else {System.out.println("Error : 3, Flavor loading problem.");}
//        if (Config.configmgr.loadScheme()){x++;} else {System.out.println("Error : 4, Flavor loading problem.");}
//        if (Config.configmgr.loadCustomerProfile()){x++;} else {System.out.println("Error : 5, CustomerProfile loading problem.");}
          if (Config.configmgr.loadMaterialType()){x++;} else {System.out.println("Error : 6, Material Type loading problem.");}  
          if (Config.configmgr.loadMaterial()){x++;} else {System.out.println("Error : 6, Material loading problem.");}
          if (Config.configmgr.loadQuantity()){x++;} else {System.out.println("Error : 7, Quantity loading problem.");}
          if (Config.configmgr.loadSupplierProfile()){x++;} else {System.out.println("Error : 8, SupplierProfile loading problem.");}
          if (Config.configmgr.loadClassManager()){x++;} else {System.out.println("Error : 9, ClassManager loading problem.");}
          if (Config.configmgr.loadForms()){x++;} else {System.out.println("Error : 10, Forms loading problem.");}
//                
        if (x!=7) {
            JOptionPane.showMessageDialog(banner,"Product initialisation problem, product can not be launch.","Error.",JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
    }
    
    private void hidebanner() {
        banner.dispose();
        Config.adminhome.setVisible(true);
//        Config.login.setVisible(true);        
    }
    
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ShapersConstruction2.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(ShapersConstruction2.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(ShapersConstruction2.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(ShapersConstruction2.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {            
            ShapersConstruction2 obj = new ShapersConstruction2();
            obj.showbanner();        
            Thread.sleep(1000);
            obj.connserver();
            Thread.sleep(1000);
            obj.conndatabase();
            Thread.sleep(1000);
            obj.initcomponent();
            Thread.sleep(1000);
            obj.hidebanner();            
        } catch (InterruptedException ex) {
            Logger.getLogger(ShapersConstruction2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
