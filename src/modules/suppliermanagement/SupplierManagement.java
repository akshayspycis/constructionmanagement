package modules.suppliermanagement;

import datamanager.Config;
import datamanager.SupplierProfile;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Vector;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.DefaultComboBoxModel;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author akshay
 */
public class SupplierManagement extends javax.swing.JDialog {
     
    DefaultTableModel tbl_smmodel;
    
    private JTextField tf = null;
    private boolean hide_flag = false;
    private Vector<String> v = new Vector<String>();
    
    public SupplierManagement(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setLocationRelativeTo(null);

        tbl_smmodel = (DefaultTableModel) tbl_SupplierManagement.getModel();    
        autosugg();
        
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        lbl_searchby = new javax.swing.JLabel();
        cb_Searchby = new javax.swing.JComboBox();
        cb_Search = new javax.swing.JComboBox();
        btn_Search = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_SupplierManagement = new javax.swing.JTable();
        btn_View = new javax.swing.JButton();
        btn_Cancel = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txt_supplierCount = new javax.swing.JTextField();
        btn_refresh = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Supplier Management");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jPanel3.setBackground(new java.awt.Color(96, 118, 159));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Supplier Management");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addContainerGap())
        );

        lbl_searchby.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lbl_searchby.setForeground(new java.awt.Color(51, 51, 51));
        lbl_searchby.setText("Search By :");

        cb_Searchby.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Name", "Org. Name", "Contact No.", "Locality", "City", "State" }));
        cb_Searchby.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_SearchbyActionPerformed(evt);
            }
        });

        cb_Search.setEditable(true);

        btn_Search.setText("Search");
        btn_Search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SearchActionPerformed(evt);
            }
        });

        jButton1.setText("New Supplier");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbl_searchby)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cb_Searchby, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cb_Search, javax.swing.GroupLayout.PREFERRED_SIZE, 646, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_Search, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel6Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lbl_searchby)
                            .addComponent(cb_Searchby, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cb_Search, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btn_Search)
                            .addComponent(jButton1))))
                .addGap(10, 10, 10))
        );

        jPanel6Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_Search, cb_Search, cb_Searchby, jButton1, jSeparator1, lbl_searchby});

        tbl_SupplierManagement.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tbl_SupplierManagement.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "SUPPLIER ID", "SUPPLIER NAME", "GENDER", "CONTACT NO.", "ORGANIZATION", "ADDRESS", "LOCALITY", "CITY", "PINCODE", "STATE"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_SupplierManagement.setRowHeight(20);
        tbl_SupplierManagement.setSelectionBackground(new java.awt.Color(96, 118, 159));
        tbl_SupplierManagement.getTableHeader().setReorderingAllowed(false);
        tbl_SupplierManagement.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_SupplierManagementMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbl_SupplierManagement);

        btn_View.setText("View");
        btn_View.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ViewActionPerformed(evt);
            }
        });

        btn_Cancel.setText("Cancel");
        btn_Cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_CancelActionPerformed(evt);
            }
        });

        jLabel1.setText("Total Supplier Count :");

        txt_supplierCount.setEditable(false);

        btn_refresh.setText("Refresh");
        btn_refresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_refreshActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btn_refresh)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_supplierCount, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_View, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_Cancel)))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_Cancel, btn_View, btn_refresh});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 463, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_Cancel)
                    .addComponent(btn_View)
                    .addComponent(jLabel1)
                    .addComponent(txt_supplierCount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_refresh))
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
  
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        Config.newsupplier.onloadReset();
        Config.newsupplier.setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void tbl_SupplierManagementMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_SupplierManagementMouseClicked
        if (evt.getClickCount()==2) {
            btn_ViewActionPerformed(null);
        }
    }//GEN-LAST:event_tbl_SupplierManagementMouseClicked

    private void btn_ViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ViewActionPerformed
        try {
            Config.viewsupplier.onloadReset(tbl_smmodel.getValueAt(tbl_SupplierManagement.getSelectedRow(), 0).toString());
            Config.viewsupplier.setVisible(true);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select any row from the table..", "No row selected", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_ViewActionPerformed

    private void btn_CancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_CancelActionPerformed
        dispose();
    }//GEN-LAST:event_btn_CancelActionPerformed

    private void btn_refreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_refreshActionPerformed
        onloadReset();
    }//GEN-LAST:event_btn_refreshActionPerformed

    private void btn_SearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SearchActionPerformed
        int searchby = cb_Searchby.getSelectedIndex();
        String searchfor;
        try {            
            searchfor = cb_Search.getSelectedItem().toString();
        } catch (Exception e) {
            searchfor = "";
        }        
        
        tbl_smmodel.setRowCount(0);        
        
        if (searchfor.equals("")) {
            for (int i = 0; i < Config.configsupplierprofile.size(); i++) {
                addRow(i);
            }            
        } else {
            switch (searchby) {
                
                case 0:                
                    for (int i = 0; i < Config.configsupplierprofile.size(); i++) {            
                        if (Config.configsupplierprofile.get(i).getSuppliername().equals(searchfor)) {
                            addRow(i);
                        }
                    }                    
                    break;
                    
                case 1:
                    for (int i = 0; i < Config.configsupplierprofile.size(); i++) {            
                        if (Config.configsupplierprofile.get(i).getOrganization().equals(searchfor)) {                
                            addRow(i);
                        }
                    }                    
                    break;

                case 2:
                    for (int i = 0; i < Config.configsupplierprofile.size(); i++) {            
                        if (Config.configsupplierprofile.get(i).getContactno().equals(searchfor)) {
                            addRow(i);
                        }
                    }
                    break;

                case 3:
                    for (int i = 0; i < Config.configsupplierprofile.size(); i++) {            
                        if (Config.configsupplierprofile.get(i).getLocality().equals(searchfor)) {
                            addRow(i);
                        }
                    }
                    break;

                case 4:
                    for (int i = 0; i < Config.configsupplierprofile.size(); i++) {            
                        if (Config.configsupplierprofile.get(i).getCity().equals(searchfor)) {
                            addRow(i);
                        }
                    }
                    break;

                case 5:
                    for (int i = 0; i < Config.configsupplierprofile.size(); i++) {            
                        if (Config.configsupplierprofile.get(i).getState().equals(searchfor)) {
                            addRow(i);
                        }
                    }
                    break;                
            }            
        }
        txt_supplierCount.setText(String.valueOf(tbl_smmodel.getRowCount()));
    }//GEN-LAST:event_btn_SearchActionPerformed

    private void cb_SearchbyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_SearchbyActionPerformed
        int searchby = cb_Searchby.getSelectedIndex();
        ArrayList<String> list = new ArrayList<String>();
        
        cb_Search.removeAllItems();
        cb_Search.addItem("");
        
        switch (searchby) {
            
            case 0:
                for (int i = 0; i < Config.configsupplierprofile.size(); i++) {
                    list.add(Config.configsupplierprofile.get(i).getSuppliername());                    
                }                
                break;
                
            case 1:
                for (int i = 0; i < Config.configsupplierprofile.size(); i++) {
                    list.add(Config.configsupplierprofile.get(i).getOrganization());
                }
                break;
                
            case 2:
                for (int i = 0; i < Config.configsupplierprofile.size(); i++) {
                    list.add(Config.configsupplierprofile.get(i).getContactno());
                }                
                break;
                
            case 3:
                for (int i = 0; i < Config.configsupplierprofile.size(); i++) {
                    list.add(Config.configsupplierprofile.get(i).getLocality());
                }
                break;
                
            case 4:
                for (int i = 0; i < Config.configsupplierprofile.size(); i++) {
                    list.add(Config.configsupplierprofile.get(i).getCity());
                }
                break;
                
            case 5:
                for (int i = 0; i < Config.configsupplierprofile.size(); i++) {
                    list.add(Config.configsupplierprofile.get(i).getState());
                }
                break;             
        }        
        Iterator it= new ArrayList(new LinkedHashSet(list)).iterator();
        while(it.hasNext()) {
            cb_Search.addItem(it.next());
        }        
    }//GEN-LAST:event_cb_SearchbyActionPerformed
  
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_Cancel;
    private javax.swing.JButton btn_Search;
    private javax.swing.JButton btn_View;
    private javax.swing.JButton btn_refresh;
    private javax.swing.JComboBox cb_Search;
    private javax.swing.JComboBox cb_Searchby;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lbl_searchby;
    private javax.swing.JTable tbl_SupplierManagement;
    private javax.swing.JTextField txt_supplierCount;
    // End of variables declaration//GEN-END:variables
 
    public void onloadReset() {
        cb_SearchbyActionPerformed(null);        
        btn_SearchActionPerformed(null);        
    } 
    
    private void addRow(int i) {
        SupplierProfile cp = Config.configsupplierprofile.get(i);
        tbl_smmodel.addRow(new Object[] {
            cp.getSupplierid(),
            cp.getSuppliername(),
            cp.getGender(),
            cp.getContactno(),
            cp.getOrganization(),
            cp.getAddress(),
            cp.getLocality(),
            cp.getCity(),                
            cp.getPincode(),
            cp.getState()                
        });
    }
    
    private void autosugg() {
        tf = (JTextField) cb_Search.getEditor().getEditorComponent();
        tf.addKeyListener(new KeyAdapter() {                
            @Override
            public void keyTyped(KeyEvent e) {            
            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    String text = tf.getText();
                    if(text.length()==0) {
                        cb_Search.hidePopup();
                        setModel(new DefaultComboBoxModel(v), "");
                    }else{
                        DefaultComboBoxModel m = getSuggestedModel(v, text);
                        if(m.getSize()==0 || hide_flag) {
                            cb_Search.hidePopup();
                            hide_flag = false;
                        }else{
                            setModel(m, text);
                            cb_Search.showPopup();
                        }
                    }
                }
            });
            }
            @Override
            public void keyPressed(KeyEvent e) {
                String text = tf.getText();
                int code = e.getKeyCode();
                if(code==KeyEvent.VK_ENTER) {
                    if(!v.contains(text)) {
                        //v.addElement(text);
                        Collections.sort(v);
                        setModel(getSuggestedModel(v, text), text);
                    }
                    hide_flag = true; 
                }else if(code==KeyEvent.VK_ESCAPE) {
                hide_flag = true; 
                }else if(code==KeyEvent.VK_RIGHT) {
                for(int i=0;i<v.size();i++) {
                    String str = v.elementAt(i);
                    if(str.startsWith(text)) {
                    cb_Search.setSelectedIndex(-1);
                    tf.setText(str);
                    return;
                    }
                }
                }
            }
        });
        
        Collections.sort(v);
        setModel(new DefaultComboBoxModel(v), "");
    }
    
    private void setModel(DefaultComboBoxModel mdl, String str) {
        cb_Search.setModel(mdl);
        cb_Search.setSelectedIndex(-1);
        tf.setText(str);
    }
    
    private DefaultComboBoxModel getSuggestedModel(java.util.List<String> list, String text) {
        DefaultComboBoxModel m = new DefaultComboBoxModel();
        for(String s: list) {
            if(s.toLowerCase().startsWith(text.toLowerCase())) {
                m.addElement(s);
            }
        }
        return m;
    }    
}
