package modules.configuration;

import datamanager.Config;
import datamanager.Material;
import datamanager.MaterialType;
import datamanager.Quantity;
import datamanager.MaterialList;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.font.TextAttribute;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;


public class ProductConfiguration extends javax.swing.JDialog {
    
    DefaultTableModel tbl_materialmodel;
    DefaultTableModel tbl_materialtypemodel;
    DefaultTableModel tbl_materiallistmodel;
    
    public ProductConfiguration(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        
        tbl_materialmodel = (DefaultTableModel) tbl_material.getModel();
        tbl_materialtypemodel = (DefaultTableModel) tbl_type.getModel();
        tbl_materiallistmodel = (DefaultTableModel) tbl_materiallist.getModel();
       
       
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });        
    }
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        jPanel8 = new javax.swing.JPanel();
        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel2 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txt_material = new javax.swing.JTextField();
        btn_addItem = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_material = new javax.swing.JTable();
        btn_productRemove = new javax.swing.JButton();
        btn_productSave = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        btn_typeAdd = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbl_type = new javax.swing.JTable();
        btn_typeRemove = new javax.swing.JButton();
        cb_materialintype = new javax.swing.JComboBox();
        txt_type = new javax.swing.JTextField();
        btn_flavorSave = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        cb_material = new javax.swing.JComboBox();
        jLabel6 = new javax.swing.JLabel();
        cb_type = new javax.swing.JComboBox();
        btn_Add = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel10 = new javax.swing.JLabel();
        cb_quantity = new javax.swing.JComboBox();
        lbl_button = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbl_materiallist = new javax.swing.JTable();
        btn_remove = new javax.swing.JButton();
        btn_schemeSave = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Product Configuration");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jPanel3.setBackground(new java.awt.Color(58, 148, 175));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Product Configuration");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));

        jSplitPane1.setBorder(null);
        jSplitPane1.setDividerLocation(496);
        jSplitPane1.setDividerSize(2);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Material", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel1.setText("Material :");

        txt_material.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_materialKeyPressed(evt);
            }
        });

        btn_addItem.setText("ADD");
        btn_addItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_addItemActionPerformed(evt);
            }
        });

        tbl_material.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tbl_material.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "MATERIAL"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_material.setRowHeight(20);
        tbl_material.setSelectionBackground(new java.awt.Color(96, 118, 159));
        tbl_material.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tbl_material);
        tbl_material.getColumnModel().getColumn(0).setResizable(false);

        btn_productRemove.setText("Remove");
        btn_productRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_productRemoveActionPerformed(evt);
            }
        });

        btn_productSave.setText("Save");
        btn_productSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_productSaveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_material, javax.swing.GroupLayout.PREFERRED_SIZE, 337, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_addItem)
                .addContainerGap())
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addGap(10, 10, 10))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btn_productRemove)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_productSave)
                .addContainerGap())
        );

        jPanel5Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_productRemove, btn_productSave});

        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel1)
                    .addComponent(txt_material, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_addItem))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 393, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_productRemove)
                    .addComponent(btn_productSave))
                .addGap(6, 6, 6))
        );

        jPanel5Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_addItem, jLabel1, txt_material});

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jSplitPane1.setLeftComponent(jPanel2);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Type", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel3.setText("Material :");

        jLabel4.setText("Type :");

        btn_typeAdd.setText("ADD");
        btn_typeAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_typeAddActionPerformed(evt);
            }
        });

        tbl_type.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tbl_type.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "TYPE"
            }
        ));
        tbl_type.setRowHeight(20);
        tbl_type.setSelectionBackground(new java.awt.Color(96, 118, 159));
        tbl_type.getTableHeader().setReorderingAllowed(false);
        jScrollPane2.setViewportView(tbl_type);
        tbl_type.getColumnModel().getColumn(0).setResizable(false);

        btn_typeRemove.setText("Remove");
        btn_typeRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_typeRemoveActionPerformed(evt);
            }
        });

        cb_materialintype.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_materialintypeActionPerformed(evt);
            }
        });

        txt_type.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_typeKeyPressed(evt);
            }
        });

        btn_flavorSave.setText("Save");
        btn_flavorSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_flavorSaveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cb_materialintype, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_type, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_typeAdd)
                .addContainerGap())
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addGap(10, 10, 10))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(btn_typeRemove)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_flavorSave)
                        .addContainerGap())))
        );

        jPanel6Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_flavorSave, btn_typeRemove});

        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3)
                        .addComponent(btn_typeAdd)
                        .addComponent(txt_type, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel4))
                    .addComponent(cb_materialintype, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 393, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_typeRemove)
                    .addComponent(btn_flavorSave))
                .addGap(6, 6, 6))
        );

        jPanel6Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_typeAdd, cb_materialintype, jLabel3, jLabel4, txt_type});

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jSplitPane1.setRightComponent(jPanel4);

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 992, Short.MAX_VALUE)
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1)
        );

        jTabbedPane2.addTab("Item Configuration", jPanel8);

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));

        jLabel5.setText("Material :");

        cb_material.setEditable(true);
        cb_material.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_materialActionPerformed(evt);
            }
        });

        jLabel6.setText("Type :");

        cb_type.setEditable(true);
        cb_type.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_typeActionPerformed(evt);
            }
        });

        btn_Add.setText("ADD");
        btn_Add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_AddActionPerformed(evt);
            }
        });

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jLabel10.setText("Quantity");

        cb_quantity.setEditable(true);

        lbl_button.setForeground(new java.awt.Color(58, 148, 175));
        lbl_button.setText("Quantity Configurable");
        lbl_button.setAutoscrolls(true);
        lbl_button.setBorder(javax.swing.BorderFactory.createCompoundBorder());
        lbl_button.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_button.setOpaque(true);
        lbl_button.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_buttonMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbl_buttonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbl_buttonMouseExited(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cb_material, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cb_type, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cb_quantity, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(24, 24, 24)
                .addComponent(lbl_button)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_Add, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btn_Add, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lbl_button, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5)
                        .addComponent(jLabel6)
                        .addComponent(cb_material, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cb_type, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel10)
                        .addComponent(cb_quantity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel9Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {cb_material, cb_type, jLabel5, jLabel6, jSeparator1});

        lbl_button.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                original = evt.getComponent().getFont();
                attributes = original.getAttributes();
                attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
                evt.getComponent().setFont(original.deriveFont(attributes));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                evt.getComponent().setFont(original);
            }
        });

        tbl_materiallist.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tbl_materiallist.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "MATERIALID", "MATERIAL", "TYPE", "QUANTITY"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                true, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_materiallist.setRowHeight(20);
        tbl_materiallist.setSelectionBackground(new java.awt.Color(96, 118, 159));
        tbl_materiallist.getTableHeader().setReorderingAllowed(false);
        jScrollPane3.setViewportView(tbl_materiallist);
        tbl_materiallist.getColumnModel().getColumn(0).setResizable(false);
        tbl_materiallist.getColumnModel().getColumn(0).setPreferredWidth(200);
        tbl_materiallist.getColumnModel().getColumn(1).setResizable(false);
        tbl_materiallist.getColumnModel().getColumn(1).setPreferredWidth(500);
        tbl_materiallist.getColumnModel().getColumn(2).setResizable(false);
        tbl_materiallist.getColumnModel().getColumn(2).setPreferredWidth(150);
        tbl_materiallist.getColumnModel().getColumn(3).setResizable(false);
        tbl_materiallist.getColumnModel().getColumn(3).setPreferredWidth(150);

        btn_remove.setText("Remove");
        btn_remove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_removeActionPerformed(evt);
            }
        });

        btn_schemeSave.setText("Save");
        btn_schemeSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_schemeSaveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(btn_remove)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_schemeSave)))
                .addContainerGap())
        );

        jPanel7Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_remove, btn_schemeSave});

        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 418, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_remove)
                    .addComponent(btn_schemeSave))
                .addContainerGap())
        );

        jTabbedPane2.addTab("Material Configuration", jPanel7);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane2)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        setBounds(0, 0, 1033, 639);
    }// </editor-fold>//GEN-END:initComponents

    //checked
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog

    //checked    
    private void btn_addItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_addItemActionPerformed
        if(!txt_material.getText().equals(""))   {
             boolean b=true;
             for (int i = 0; i < tbl_material.getRowCount(); i++) {
             if(txt_material.getText().equals(tbl_material.getValueAt(i, 0).toString()))    
             {
             b=false;
             break;
             }   
             }
          if(b){
          Material q=new Material();
           
          q.setMaterialname(txt_material.getText().substring(0, 1).toUpperCase() + txt_material.getText().substring(1));
          
          if(Config.materialmgr.insMaterial(q)){
           onloadResetMaterial();
                 JOptionPane.showMessageDialog(this,"Material configure successfully.","Configure Successful.",JOptionPane.NO_OPTION);
                 txt_material.setText("");
                 txt_material.requestFocus();
            } else {
                 JOptionPane.showMessageDialog(this,"Material in configure process.","Error",JOptionPane.ERROR_MESSAGE);
                 txt_material.setText("");
                 txt_material.requestFocus();
            }            
          }else{
                JOptionPane.showMessageDialog(this,"Value already exits.","Error",JOptionPane.ERROR_MESSAGE); 
                txt_material.setText("");
                 txt_material.requestFocus();
          }   
         }else{
              JOptionPane.showMessageDialog(this,"Material should not be blank","Error",JOptionPane.ERROR_MESSAGE);
              txt_material.setText("");
                 txt_material.requestFocus();
         }
    }//GEN-LAST:event_btn_addItemActionPerformed

    //checked
    private void btn_typeAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_typeAddActionPerformed
        String type;
        try {
            type = txt_type.getText().trim().toUpperCase();
        } catch (Exception e) {
            type = "";
        }
        
        if (!type.equals("")) {        
            int i;
            for (i = 0; i < tbl_materiallistmodel.getRowCount(); i++) {
                if (type.equals(tbl_materiallistmodel.getValueAt(i, 0).toString())) {
                    break;
                }
            }

            if (i == tbl_materiallistmodel.getRowCount()) {
                MaterialType mt =new MaterialType();
                mt.setMaterialid(Config.configmaterial.get(cb_materialintype.getSelectedIndex()).getMaterialid());
                mt.setType(type);
                if(Config.materialtypemgr.insMaterialType(mt)){
                cb_materialintype.setSelectedIndex(cb_materialintype.getSelectedIndex());    
                JOptionPane.showMessageDialog(this,"Type configure successfully.","Configure Successful.",JOptionPane.NO_OPTION);
                txt_type.setText("");
                txt_type.requestFocus();
            } else {
                 JOptionPane.showMessageDialog(this,"Error in configure process.","Error",JOptionPane.ERROR_MESSAGE);
                 txt_type.setText("");
                txt_type.requestFocus();
            }  
            } else {
                JOptionPane.showMessageDialog(this, "Entry already exits.", "Multiple entry", JOptionPane.ERROR_MESSAGE);
                txt_type.setText("");
                txt_type.requestFocus();
            }
        } else {
            JOptionPane.showMessageDialog(this, "Enter something to add.", "No Data Found", JOptionPane.ERROR_MESSAGE);
            txt_type.setText("");
            txt_type.requestFocus();
        }
    }//GEN-LAST:event_btn_typeAddActionPerformed

    //checked
    private void btn_productRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_productRemoveActionPerformed
        try {
            if(Config.materialmgr.delMaterial(Config.configmaterial.get(tbl_material.getSelectedRow()).getMaterialid())){
                onloadResetMaterial();
                JOptionPane.showMessageDialog(this,"Material delete successfully.","Configure Successful.",JOptionPane.NO_OPTION);
            } else {
                 JOptionPane.showMessageDialog(this,"Error in delete process.","Error",JOptionPane.ERROR_MESSAGE);
            }  

        } catch (Exception e) {            
            JOptionPane.showMessageDialog(this, "Select any row from the table.", "No row selected.", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_productRemoveActionPerformed

    //checked
    private void btn_typeRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_typeRemoveActionPerformed
 try {
     String id="";
      for (int i = 0; i < Config.configmaterialTypes.size(); i++) {
         MaterialType m = Config.configmaterialTypes.get(i);
          if(m.getMaterialid().equals(Config.configmaterial.get(cb_materialintype.getSelectedIndex()).getMaterialid()) && m.getType().equals(tbl_materialtypemodel.getValueAt(tbl_type.getSelectedRow(), 0).toString()) )
          {
              id=m.getTypeid();
              break;
          }  
          
     }
     
            if(Config.materialtypemgr.delMaterialType(id)){
                cb_materialintype.setSelectedIndex(cb_materialintype.getSelectedIndex());
                JOptionPane.showMessageDialog(this,"Type delete successfully.","Configure Successful.",JOptionPane.NO_OPTION);
                cb_materialintypeActionPerformed(null);
            } else {
                 JOptionPane.showMessageDialog(this,"Error in delete process.","Error",JOptionPane.ERROR_MESSAGE);
            }  

        } catch (Exception e) {       
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, "Select any row from the table.", "No row selected.", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_typeRemoveActionPerformed

    //checked
    private void cb_materialintypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_materialintypeActionPerformed
        try {
        tbl_materialtypemodel.setRowCount(0);
        for (int i = 0; i < Config.configmaterialTypes.size(); i++) {
            MaterialType m = Config.configmaterialTypes.get(i);
            if(Config.configmaterial.get(cb_materialintype.getSelectedIndex()).getMaterialid().equals(m.getMaterialid())){
            tbl_materialtypemodel.addRow(new Object[] { m.getType() });
            }
        }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_cb_materialintypeActionPerformed

    //checked
    private void btn_AddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_AddActionPerformed
        try {
        MaterialList ml= new MaterialList();
        ml.setMaterialid(Config.configmaterial.get(cb_material.getSelectedIndex()).getMaterialid());

        for (int i = 0; i < Config.configmaterialTypes.size(); i++) {
            if(ml.getMaterialid().equals( Config.configmaterialTypes.get(i).getMaterialid()) && cb_type.getSelectedItem().toString().equals( Config.configmaterialTypes.get(i).getType()))    
            {   
                ml.setTypeid( Config.configmaterialTypes.get(i).getTypeid());
                break;
            }   
        }
        ml.setQuantityid(Config.configquantity.get(cb_quantity.getSelectedIndex()).getQuantityid());    
        
        if(Config.materiallistmgr.insMaterialList(ml)){
                onloadResetMaterialList();
                JOptionPane.showMessageDialog(this,"Material list configure successfully.","Configure Successful.",JOptionPane.NO_OPTION);
                cb_material.requestFocus();
            } else {
               JOptionPane.showMessageDialog(this,"Error in configure process.","Error",JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
               JOptionPane.showMessageDialog(this,"Error in configure process.","Error",JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_AddActionPerformed

    //checked
    private void cb_typeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_typeActionPerformed
//        try {
//            tbl_schememodel.setRowCount(0);
//            String product = cb_material.getSelectedItem().toString();
//            String flavor;
//            try {
//                flavor = cb_type.getSelectedItem().toString();
//            } catch (Exception e) {
//                flavor = "";
//            }            
//
//            for (int i = 0; i < Config.configscheme.size(); i++) {
//                Scheme scheme = Config.configscheme.get(i);            
//                if(scheme.getProduct().equals(product) && scheme.getFlavor().equals(flavor)) {
//                   tbl_schememodel.addRow(new Object[] {
//                       scheme.getMrp(),
//                       scheme.getAmount(),
//                       scheme.getPercentage()
//                   });
//                }
//            }
//        } catch (Exception e) {}
    }//GEN-LAST:event_cb_typeActionPerformed

    //checked
    private void cb_materialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_materialActionPerformed
try {
        cb_type.removeAllItems();
        int j=0;
        for (int i = 0; i < Config.configmaterialTypes.size(); i++) {
            MaterialType m = Config.configmaterialTypes.get(i);
            if(Config.configmaterial.get(cb_material.getSelectedIndex()).getMaterialid().equals(m.getMaterialid())){
            cb_type.addItem(m.getType());
            }
        }
        } catch (Exception e) {}   
    }//GEN-LAST:event_cb_materialActionPerformed

    //checked
    private void btn_removeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_removeActionPerformed
        try {
            
            if(Config.materiallistmgr.delMaterialList(Config.configmateriallist.get(tbl_materiallist.getSelectedRow()).getMateriallistid())){
            JOptionPane.showMessageDialog(this,"Material list delete successfully.","Configure Successful.",JOptionPane.NO_OPTION);    
            onloadResetMaterialList();    
            }else{
                JOptionPane.showMessageDialog(this, "Error in deletion.", "No row selected.", JOptionPane.ERROR_MESSAGE);
            }
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select any row from the table.", "No row selected.", JOptionPane.ERROR_MESSAGE);
        }        
    }//GEN-LAST:event_btn_removeActionPerformed

    //checked
    private void btn_productSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_productSaveActionPerformed
//        Product[] product = new Product[tbl_productmodel.getRowCount()];
//        int i;
//        try {
//            for (i = 0; i < product.length; i++) {
//                product[i] = new Product();
//                product[i].setProductname(tbl_productName.getValueAt(i, 0).toString());
//            }
//            
//            if (Config.productmgr.insProduct(product)) {
//                onloadReset();
//                JOptionPane.showMessageDialog(this,"Products configure successfully.","Configure Successful.",JOptionPane.NO_OPTION);
//            } else {
//                JOptionPane.showMessageDialog(this,"Problem in configure process.","Error",JOptionPane.ERROR_MESSAGE);
//            }            
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            JOptionPane.showMessageDialog(this,ex.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
//        }
    }//GEN-LAST:event_btn_productSaveActionPerformed

    //checked
    private void btn_flavorSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_flavorSaveActionPerformed
//        Flavor[] flavor = new Flavor[tbl_flavormodel.getRowCount()];
//        int i;
//        try {
//            for (i = 0; i < flavor.length; i++) {
//                flavor[i] = new Flavor();
//                flavor[i].setFlavorname(tbl_flavorName.getValueAt(i, 0).toString());                
//            }
//            
//            if (Config.flavormgr.insFlavor(flavor, cb_Product.getSelectedItem().toString())) {
//                onloadReset();
//                JOptionPane.showMessageDialog(this,"Flavors configure successfully.","Configure Successful.",JOptionPane.NO_OPTION);
//            } else {
//                JOptionPane.showMessageDialog(this,"Problem in configure process.","Error",JOptionPane.ERROR_MESSAGE);
//            }            
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            JOptionPane.showMessageDialog(this,ex.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
//        }
    }//GEN-LAST:event_btn_flavorSaveActionPerformed

    //checked
    private void btn_schemeSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_schemeSaveActionPerformed
//        Scheme[] scheme = new Scheme[tbl_schememodel.getRowCount()];
//        
//        String product = cb_material.getSelectedItem().toString();
//        String flavor;
//        try {
//            flavor = cb_type.getSelectedItem().toString();
//        } catch (Exception e) {
//            flavor = "";
//        }
//        
//        int i;
//        try {
//            for (i = 0; i < scheme.length; i++) {
//                scheme[i] = new Scheme();
//                scheme[i].setMrp(tbl_scheme.getValueAt(i, 0).toString());
//                scheme[i].setAmount(tbl_scheme.getValueAt(i, 1).toString());
//                scheme[i].setPercentage(tbl_scheme.getValueAt(i, 2).toString());
//            }
//            
//            if (Config.schememgr.insScheme(scheme, product, flavor)) {
//                onloadReset();
//                JOptionPane.showMessageDialog(this,"Scheme configure successfully.","Configure Successful.",JOptionPane.NO_OPTION);
//            } else {
//                JOptionPane.showMessageDialog(this,"Problem in configure process.","Error",JOptionPane.ERROR_MESSAGE);
//            }            
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            JOptionPane.showMessageDialog(this,ex.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
//        }
    }//GEN-LAST:event_btn_schemeSaveActionPerformed

    private void lbl_buttonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_buttonMouseClicked
        Config.quantitymanagement.onloadReset();
        Config.quantitymanagement.setVisible(true);
    }//GEN-LAST:event_lbl_buttonMouseClicked

    private void lbl_buttonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_buttonMouseEntered
     // TODO add your handling code here:
    }//GEN-LAST:event_lbl_buttonMouseEntered

    private void lbl_buttonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_buttonMouseExited
          // TODO add your handling code here:
    }//GEN-LAST:event_lbl_buttonMouseExited

    private void txt_typeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_typeKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            btn_typeAdd.requestFocus();
            btn_typeAddActionPerformed(null);
        }
        if(evt.getKeyCode()==KeyEvent.VK_LEFT){
            txt_material.requestFocus();
        }
    }//GEN-LAST:event_txt_typeKeyPressed

    private void txt_materialKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_materialKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            btn_addItemActionPerformed(null);
        }
        if(evt.getKeyCode()==KeyEvent.VK_RIGHT){
            txt_type.requestFocus();
        }
    }//GEN-LAST:event_txt_materialKeyPressed
        
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_Add;
    private javax.swing.JButton btn_addItem;
    private javax.swing.JButton btn_flavorSave;
    private javax.swing.JButton btn_productRemove;
    private javax.swing.JButton btn_productSave;
    private javax.swing.JButton btn_remove;
    private javax.swing.JButton btn_schemeSave;
    private javax.swing.JButton btn_typeAdd;
    private javax.swing.JButton btn_typeRemove;
    private javax.swing.JComboBox cb_material;
    private javax.swing.JComboBox cb_materialintype;
    private javax.swing.JComboBox cb_quantity;
    private javax.swing.JComboBox cb_type;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JLabel lbl_button;
    private java.awt.Font original;
    private java.util.Map attributes;
    private javax.swing.JTable tbl_material;
    private javax.swing.JTable tbl_materiallist;
    private javax.swing.JTable tbl_type;
    private javax.swing.JTextField txt_material;
    private javax.swing.JTextField txt_type;
    // End of variables declaration//GEN-END:variables
   
    //checked
    public void onloadReset() {
        onloadResetMaterial();
   //     onloadResetMaterialType();
        onloadResetMaterialList();
        onloadResetQuantity();
    }
    
    public void onloadResetMaterialList(){
         try {
          tbl_materiallistmodel.setRowCount(0);
          
        for (int i = 0; i < Config.configmateriallist.size(); i++) {
            String type = "";
            String material = "";
            String quantity = "";
        
            MaterialList m =Config.configmateriallist.get(i);
              for (int j = 0; j < Config.configmaterial.size(); j++) {
                if(Config.configmaterial.get(j).getMaterialid().equals(m.getMaterialid())){
                    material=Config.configmaterial.get(j).getMaterialname();
                break;    
                }
            }
          for (int k = 0; k < Config.configmaterialTypes.size(); k++) {
                if(Config.configmaterialTypes.get(k).getTypeid().equals(m.getTypeid())){
                    type=Config.configmaterialTypes.get(k).getType();
                break;    
                }
            }
            for (int l = 0; l < Config.configquantity.size(); l++) {
                if(Config.configquantity.get(l).getQuantityid().equals(m.getQuantityid())){
                    quantity=Config.configquantity.get(l).getQuantitytype();
                break;    
                }
            }
         
              tbl_materiallistmodel.addRow(new Object[] { 
                 m.getMateriallistid(),
                 material,
                 type,
                 quantity
              });
        }  
       } catch (Exception e) {
           e.printStackTrace();
       }           
       }

    public void onloadResetMaterial(){
        tbl_materialmodel.setRowCount(0);
        cb_material.removeAllItems();
        cb_materialintype.removeAllItems();
        for (int i = 0; i < Config.configmaterial.size(); i++) {
            Material m = Config.configmaterial.get(i);
            tbl_materialmodel.addRow(new Object[] { m.getMaterialname() });
            cb_material.addItem(m.getMaterialname());
            cb_materialintype.addItem(m.getMaterialname());
        }
   
    }
    
    public void onloadResetQuantity(){
     cb_quantity.removeAllItems();
        for (int i = 0; i < Config.configquantity.size(); i++) {
            cb_quantity.addItem(Config.configquantity.get(i).getQuantitytype());
        }
    }
 
    

    
    
    
    
}