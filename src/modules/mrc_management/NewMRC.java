/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modules.mrc_management;

import classmanager.MRCDetailsMgr;
import datamanager.Config;
import datamanager.MRC_Details;
import datamanager.MaterialList;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.DefaultComboBoxModel;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

/**
 *
 * @author akshay
 */
public class NewMRC extends javax.swing.JDialog {
    
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");    
    SimpleDateFormat sdf_time = new SimpleDateFormat("H:mm:ss ");    
    SimpleDateFormat sdf_am_pm = new SimpleDateFormat("a");    
    
    public NewMRC(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
     this.setLocationRelativeTo(null);
     
      // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });   
     
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jComboBox1 = new javax.swing.JComboBox();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txt_site = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txt_no = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jdc_date = new com.toedter.calendar.JDateChooser();
        jLabel4 = new javax.swing.JLabel();
        cb_supplier_name = new javax.swing.JComboBox();
        jLabel5 = new javax.swing.JLabel();
        cb_material = new javax.swing.JComboBox();
        jPanel2 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        txt_length = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txt_width = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txt_height = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txt_qty = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jtc_time = new lu.tudor.santec.jtimechooser.JTimeChooser();
        jLabel13 = new javax.swing.JLabel();
        txt_truckno = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        txt_royaltyno = new javax.swing.JTextField();
        cb_AmPm = new javax.swing.JComboBox();
        jButton1 = new javax.swing.JButton();
        btn_reset = new javax.swing.JButton();
        btn_cancel = new javax.swing.JButton();
        btn_save = new javax.swing.JButton();

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Material Receipt", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel1.setText("Site :");

        txt_site.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_siteActionPerformed(evt);
            }
        });
        txt_site.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_siteKeyPressed(evt);
            }
        });

        jLabel2.setText("No. :");

        jLabel3.setText("Date :");

        jLabel4.setText("Supplier :");

        cb_supplier_name.setEditable(true);
        cb_supplier_name.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_supplier_nameActionPerformed(evt);
            }
        });

        jLabel5.setText("Materials :");

        cb_material.setEditable(true);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Measurement"));
        jPanel2.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                jPanel2AncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });

        jLabel8.setText("Length ");

        txt_length.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_lengthCaretUpdate(evt);
            }
        });
        txt_length.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txt_lengthFocusGained(evt);
            }
        });

        jLabel9.setText("Width");

        txt_width.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_widthCaretUpdate(evt);
            }
        });
        txt_width.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txt_widthFocusGained(evt);
            }
        });

        jLabel10.setText("Height");

        txt_height.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_heightCaretUpdate(evt);
            }
        });
        txt_height.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txt_heightFocusGained(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txt_length, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txt_width)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txt_height, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txt_height, txt_length});

        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10)
                    .addComponent(txt_length, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_width, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_height, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel11.setText("Quantity:");

        jLabel12.setText("Time  :");

        jLabel13.setText("Truck No.");

        jLabel14.setText("Royalty No.");

        cb_AmPm.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "AM", "PM" }));
        cb_AmPm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_AmPmActionPerformed(evt);
            }
        });

        jButton1.setText("New ");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel13)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txt_truckno, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel14))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel11)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txt_qty, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel12)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_royaltyno)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jtc_time, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cb_AmPm, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cb_material, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cb_supplier_name, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(21, 21, 21)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_site)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(txt_no, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jdc_date, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txt_site, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2)
                        .addComponent(txt_no, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel3))
                    .addComponent(jdc_date, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(cb_supplier_name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(cb_material, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel11)
                        .addComponent(txt_qty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel12))
                    .addComponent(jtc_time, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cb_AmPm, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(txt_truckno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14)
                    .addComponent(txt_royaltyno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {cb_material, txt_no});

        jPanel3Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {cb_supplier_name, jButton1});

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btn_reset.setText("Reset");
        btn_reset.setPreferredSize(new java.awt.Dimension(63, 23));
        btn_reset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_resetActionPerformed(evt);
            }
        });

        btn_cancel.setText("Cancel");
        btn_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelActionPerformed(evt);
            }
        });

        btn_save.setText("Save");
        btn_save.setPreferredSize(new java.awt.Dimension(67, 23));
        btn_save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_saveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btn_reset, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_save, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_cancel)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_reset, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_cancel)
                    .addComponent(btn_save, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_resetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_resetActionPerformed
        onloadReset();
    }//GEN-LAST:event_btn_resetActionPerformed

    private void txt_siteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_siteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_siteActionPerformed

    private void jPanel2AncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_jPanel2AncestorAdded
        // TODO add your handling code here:
    }//GEN-LAST:event_jPanel2AncestorAdded

    private void btn_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelActionPerformed
        dispose();
    }//GEN-LAST:event_btn_cancelActionPerformed

    private void btn_saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_saveActionPerformed
        String str =checkValidation();
        if(str.equals("ok")){
        MRC_Details md =new MRC_Details();

        md.setMrc_site(txt_site.getText().toUpperCase());
        md.setMrc_no(txt_no.getText());
        try {md.setMrc_date(sdf.format(jdc_date.getDate()));} catch (Exception e) {}
        md.setSupplierid(Config.configsupplierprofile.get(cb_supplier_name.getSelectedIndex()).getSupplierid());
        md.setMatriallistid(Config.configmateriallist.get(cb_material.getSelectedIndex()).getMateriallistid());
        md.setMrc_length(txt_length.getText());
        md.setMrc_width(txt_width.getText());
        md.setMrc_height(txt_height.getText());
            try {
                md.setMrc_measurement( String.format("%.2f",(Float.parseFloat(txt_length.getText()) * Float.parseFloat(txt_width.getText()) *  Float.parseFloat(txt_height.getText()) ) / (1728) ) );
            } catch (Exception e) {
                md.setMrc_measurement("");
            }
        md.setMrc_quantity(txt_qty.getText());
        md.setMrc_time(sdf_time.format(jtc_time.getDateWithTime(new Date())));
        md.setMrc_am_pm(cb_AmPm.getSelectedItem().toString());
        md.setMrc_truckno(txt_truckno.getText());
        md.setMrc_royaltyno(txt_royaltyno.getText());

        MRCDetailsMgr mg =new MRCDetailsMgr();
        
        if(mg.insMRC_Details(md)){
            Config.mrc_details_management.onloadReset();
            JOptionPane.showMessageDialog(this,"Quantity Type Update successfully.","Configure Successful.",JOptionPane.NO_OPTION);
                 dispose();
        }else{
              JOptionPane.showMessageDialog(this,"Problem in configure process.","Error",JOptionPane.ERROR_MESSAGE);
        }
        }else{
             JOptionPane.showMessageDialog(this, "Check '"+str+"' field should not be blank.", "Error", JOptionPane.ERROR_MESSAGE);
        } 
    }//GEN-LAST:event_btn_saveActionPerformed

    private void txt_lengthCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_lengthCaretUpdate
        try {
            if(!(txt_length.getText().length()==0)){
              Float.parseFloat(txt_length.getText());
            }
        } catch (Exception e) {
            Toolkit.getDefaultToolkit().beep();
            JOptionPane.showConfirmDialog(null, "Please enter numbers only", "naughty", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_txt_lengthCaretUpdate

    private void txt_widthCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_widthCaretUpdate
         try {
            if(!txt_width.getText().equals("")){
              Float.parseFloat(txt_width.getText());
            }
        } catch (Exception e) {
            Toolkit.getDefaultToolkit().beep();
            JOptionPane.showConfirmDialog(null, "Please enter numbers only", "naughty", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_txt_widthCaretUpdate

    private void txt_heightCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_heightCaretUpdate
         try {
            if(!txt_height.getText().equals("")){
              Float.parseFloat(txt_height.getText());
            }
        } catch (Exception e) {
            Toolkit.getDefaultToolkit().beep();
            JOptionPane.showConfirmDialog(null, "Please enter numbers only", "naughty", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_txt_heightCaretUpdate

    private void txt_lengthFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_lengthFocusGained
        txt_length.setText("");
    }//GEN-LAST:event_txt_lengthFocusGained

    private void txt_widthFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_widthFocusGained
        txt_width.setText("");
    }//GEN-LAST:event_txt_widthFocusGained

    private void txt_heightFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_heightFocusGained
        txt_height.setText("");
    }//GEN-LAST:event_txt_heightFocusGained

    private void txt_siteKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_siteKeyPressed
         
    }//GEN-LAST:event_txt_siteKeyPressed

    private void cb_AmPmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_AmPmActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cb_AmPmActionPerformed

    private void cb_supplier_nameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_supplier_nameActionPerformed
       
    }//GEN-LAST:event_cb_supplier_nameActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        Config.newsupplier.onloadReset();
        Config.newsupplier.setVisible(true);       
    }//GEN-LAST:event_jButton1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cancel;
    private javax.swing.JButton btn_reset;
    private javax.swing.JButton btn_save;
    private javax.swing.JComboBox cb_AmPm;
    private javax.swing.JComboBox cb_material;
    private javax.swing.JComboBox cb_supplier_name;
    private javax.swing.JButton jButton1;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private com.toedter.calendar.JDateChooser jdc_date;
    private lu.tudor.santec.jtimechooser.JTimeChooser jtc_time;
    private javax.swing.JTextField txt_height;
    private javax.swing.JTextField txt_length;
    private javax.swing.JTextField txt_no;
    private javax.swing.JTextField txt_qty;
    private javax.swing.JTextField txt_royaltyno;
    private javax.swing.JTextField txt_site;
    private javax.swing.JTextField txt_truckno;
    private javax.swing.JTextField txt_width;
    // End of variables declaration//GEN-END:variables

    public void onloadReset() {     
        txt_site.setText("");
        txt_no.setText("");
        jdc_date.setDate(null);

        onloadSupplier();
        onloadMaterial();
        
        txt_length.setText("");
        txt_width.setText("");
        txt_height.setText("");
        txt_qty.setText("");
        
        try {
            jtc_time.setTime(new Date());
        } catch (Exception ex) {ex.printStackTrace();
        }
        cb_AmPm.setSelectedItem(sdf_am_pm.format(jtc_time.getDateWithTime(new Date())));
        txt_truckno.setText("");
        txt_royaltyno.setText("");
    }

    public String checkValidation() {        
        if(txt_site.getText() .equals("")){
            return "site";
        }else if(txt_no.getText().equals("")){
            return "RMC no";
        }else if(jdc_date.getDate()==null){
            return "DATE";
        }else if(cb_supplier_name.getSelectedIndex()==-1){
            return "Supplier name";
        }else if(cb_material.getSelectedIndex()==-1){
            return "Material";
        }else if(txt_qty.equals("")){
            return "site";
        }else if(txt_royaltyno.equals("")){
            return "Royalty No";
        }else{
            return "ok";
        }
    }

    public void onloadSupplier() {
        cb_supplier_name.removeAllItems();        
        for (int i = 0; i < Config.configsupplierprofile.size(); i++) {
        cb_supplier_name.addItem(Config.configsupplierprofile.get(i).getSuppliername());
      }
    }
    
   public void onloadMaterial(){
         cb_material.removeAllItems(); 
                for (int j = 0; j < Config.configmateriallist.size(); j++) {
                     cb_material.addItem(searchItem(Config.configmateriallist.get(j).getMaterialid(), "material")+"   " +searchItem(Config.configmateriallist.get(j).getTypeid(), "materialtype")+"   ("+searchItem(Config.configmateriallist.get(j).getQuantityid(), "quantity")+")");
                     
                 }  
    }

    public String searchItem(String value,String name){
        if(name.equals("material")){
        for (int i = 0; i < Config.configmaterial.size(); i++) {
          if(Config.configmaterial.get(i).getMaterialid().equals(value)){
              return Config.configmaterial.get(i).getMaterialname();
          }
        }
        
       }
        if(name.equals("materialtype")){
        for (int i = 0; i < Config.configmaterialTypes.size(); i++) {
          if(Config.configmaterialTypes.get(i).getTypeid().equals(value)){
              return Config.configmaterialTypes.get(i).getType();
          }
        }
        
       } if(name.equals("quantity")){
        for (int i = 0; i < Config.configquantity.size(); i++) {
          if(Config.configquantity.get(i).getQuantityid().equals(value)){
              return Config.configquantity.get(i).getQuantitytype();
          }
        }   
        
     }
       return "";
    }
    
    
    }

