-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.0.13-rc-nt


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema shapersconstruction2
--

CREATE DATABASE IF NOT EXISTS shapersconstruction2;
USE shapersconstruction2;

--
-- Definition of table `material`
--

DROP TABLE IF EXISTS `material`;
CREATE TABLE `material` (
  `materialid` int(10) unsigned NOT NULL auto_increment,
  `materialname` varchar(200) default NULL,
  PRIMARY KEY  (`materialid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `material`
--

/*!40000 ALTER TABLE `material` DISABLE KEYS */;
INSERT INTO `material` (`materialid`,`materialname`) VALUES 
 (4,'Agg'),
 (5,'Dust'),
 (6,'Send'),
 (7,'Cement'),
 (8,'Lime'),
 (9,'Bricks'),
 (10,'Steel'),
 (12,'Binding wire'),
 (13,'Diesel'),
 (14,'Oil'),
 (15,'Black polythin'),
 (16,'Rcc pipe'),
 (17,'Red stone'),
 (18,'Fancing pol'),
 (19,'Paving block'),
 (20,'Admixture drum'),
 (21,'Joint filler'),
 (23,'Sika powder'),
 (26,'Joint sheet');
/*!40000 ALTER TABLE `material` ENABLE KEYS */;


--
-- Definition of table `materiallist`
--

DROP TABLE IF EXISTS `materiallist`;
CREATE TABLE `materiallist` (
  `materiallistid` int(10) unsigned NOT NULL auto_increment,
  `materialid` varchar(200) default NULL,
  `typeid` varchar(200) default NULL,
  `quantityid` varchar(200) default NULL,
  PRIMARY KEY  (`materiallistid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `materiallist`
--

/*!40000 ALTER TABLE `materiallist` DISABLE KEYS */;
INSERT INTO `materiallist` (`materiallistid`,`materialid`,`typeid`,`quantityid`) VALUES 
 (32,'4','62','45'),
 (33,'4','63','45'),
 (34,'5',NULL,'45'),
 (35,'7',NULL,'46'),
 (36,'8',NULL,'46'),
 (37,'9',NULL,'47'),
 (38,'10','46','48'),
 (39,'10','47','48'),
 (40,'10','48','48'),
 (41,'10','49','48'),
 (42,'10','50','48'),
 (43,'10','51','48'),
 (44,'12',NULL,'48'),
 (45,'13',NULL,'49'),
 (46,'14','52','49'),
 (47,'14','53','49'),
 (48,'14','54','49'),
 (49,'14','55','49'),
 (50,'14','56','49'),
 (51,'14','57','49'),
 (52,'15',NULL,'47'),
 (53,'16','58','47'),
 (54,'16','59','47'),
 (55,'16','60','47'),
 (56,'16','61','47'),
 (57,'17',NULL,'50'),
 (58,'18',NULL,'47'),
 (59,'19',NULL,'47'),
 (60,'20',NULL,'47'),
 (62,'23',NULL,'48'),
 (63,'26',NULL,'47'),
 (64,'21',NULL,'51');
/*!40000 ALTER TABLE `materiallist` ENABLE KEYS */;


--
-- Definition of table `materialtype`
--

DROP TABLE IF EXISTS `materialtype`;
CREATE TABLE `materialtype` (
  `typeid` int(10) unsigned NOT NULL auto_increment,
  `materialid` varchar(200) default NULL,
  `type` varchar(200) default NULL,
  PRIMARY KEY  (`typeid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `materialtype`
--

/*!40000 ALTER TABLE `materialtype` DISABLE KEYS */;
INSERT INTO `materialtype` (`typeid`,`materialid`,`type`) VALUES 
 (46,'10','25MM'),
 (47,'10','20M'),
 (48,'10','16MM'),
 (49,'10','12MM'),
 (50,'10','10MM'),
 (51,'10','08MM'),
 (52,'14','ENGINE'),
 (53,'14','HYDARULIC'),
 (54,'14','TRANSMISSION'),
 (55,'14','CENTERING'),
 (56,'14','G-OIL'),
 (57,'14','GREESH'),
 (58,'16','250MM'),
 (59,'16','300MM'),
 (60,'16','450'),
 (61,'16','1000MM'),
 (62,'4','40,20,10MM'),
 (63,'4','MIX'),
 (64,'4','6MM');
/*!40000 ALTER TABLE `materialtype` ENABLE KEYS */;


--
-- Definition of table `quantity`
--

DROP TABLE IF EXISTS `quantity`;
CREATE TABLE `quantity` (
  `quantityid` int(10) unsigned NOT NULL auto_increment,
  `quantitytype` varchar(200) default NULL,
  PRIMARY KEY  (`quantityid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quantity`
--

/*!40000 ALTER TABLE `quantity` DISABLE KEYS */;
INSERT INTO `quantity` (`quantityid`,`quantitytype`) VALUES 
 (45,'Cft'),
 (46,'Bag'),
 (47,'Nos'),
 (48,'Kg'),
 (49,'Lt'),
 (50,'Sqft'),
 (51,'Ltr');
/*!40000 ALTER TABLE `quantity` ENABLE KEYS */;


--
-- Definition of table `supplierprofile`
--

DROP TABLE IF EXISTS `supplierprofile`;
CREATE TABLE `supplierprofile` (
  `supplierid` int(10) unsigned NOT NULL auto_increment,
  `suppliername` varchar(200) default NULL,
  `gender` varchar(200) default NULL,
  `contactno` varchar(200) default NULL,
  `organization` varchar(200) default NULL,
  `tinno` varchar(200) default NULL,
  `address` varchar(200) default NULL,
  `locality` varchar(200) default NULL,
  `city` varchar(200) default NULL,
  `state` varchar(200) default NULL,
  `pincode` varchar(200) default NULL,
  `email` varchar(200) default NULL,
  `fax` varchar(200) default NULL,
  `website` varchar(200) default NULL,
  `other` varchar(200) default NULL,
  PRIMARY KEY  (`supplierid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supplierprofile`
--

/*!40000 ALTER TABLE `supplierprofile` DISABLE KEYS */;
INSERT INTO `supplierprofile` (`supplierid`,`suppliername`,`gender`,`contactno`,`organization`,`tinno`,`address`,`locality`,`city`,`state`,`pincode`,`email`,`fax`,`website`,`other`) VALUES 
 (1,'hjl','Male','+91 8817919016','l','jl','j','j','jj','Jammu and Kashmir','j','j','jj','jl','jj'),
 (2,'akaskj','Male','+91 8817919046','jk','k','jk','jk','jk','Jharkhand','jk','kjk','jk','jk','jkj');
/*!40000 ALTER TABLE `supplierprofile` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
